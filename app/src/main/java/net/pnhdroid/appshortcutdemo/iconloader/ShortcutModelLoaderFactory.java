package net.pnhdroid.appshortcutdemo.iconloader;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;

import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;

import net.pnhdroid.appshortcutdemo.data.ShortcutInfo;

public class ShortcutModelLoaderFactory implements ModelLoaderFactory<ShortcutInfo, Drawable> {

    private final Context mContext;

    ShortcutModelLoaderFactory(Context context ) {
        mContext = context;
    }

    @NonNull
    @Override
    public ModelLoader<ShortcutInfo, Drawable> build(@NonNull MultiModelLoaderFactory multiFactory) {
        return new ShortcutModelLoader( mContext );
    }

    @Override
    public void teardown() {
        // Empty Implementation.
    }
}
