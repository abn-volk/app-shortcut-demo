package net.pnhdroid.appshortcutdemo.iconloader;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.DataFetcher;

import net.pnhdroid.appshortcutdemo.data.ShortcutInfo;

public class ShortcutDataFetcher implements DataFetcher<Drawable> {

    private final ShortcutInfo mModel;
    private final Context mContext;


    ShortcutDataFetcher(Context context, ShortcutInfo model) {
        mModel = model;
        mContext = context;
    }

    @Override
    public void loadData(@NonNull Priority priority, @NonNull DataCallback<? super Drawable> callback) {
        PackageManager pm = mContext.getPackageManager();
        try {
            Resources res = pm.getResourcesForApplication(mModel.getPackageName());
            callback.onDataReady(res.getDrawable(mModel.getIconRes()));

        } catch (PackageManager.NameNotFoundException e) {
            callback.onLoadFailed(e);
        }

    }

    @Override
    public void cleanup() {
        // Empty Implementation
    }

    @Override
    public void cancel() {
        // Empty Implementation
    }

    @NonNull
    @Override
    public Class<Drawable> getDataClass() {
        return Drawable.class;
    }

    @NonNull
    @Override
    public DataSource getDataSource() {
        return DataSource.LOCAL;
    }
}
