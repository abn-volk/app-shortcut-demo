package net.pnhdroid.appshortcutdemo.iconloader;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.DataFetcher;

public class DrawableDataFetcher implements DataFetcher<Drawable> {

    private final ActivityInfo mModel;
    private final Context mContext;


    DrawableDataFetcher(Context context, ActivityInfo model) {
        mModel = model;
        mContext = context;
    }

    @Override
    public void loadData(@NonNull Priority priority, @NonNull DataCallback<? super Drawable> callback) {
        final Drawable icon = mModel.loadIcon(mContext.getPackageManager());

        callback.onDataReady( icon );
    }

    @Override
    public void cleanup() {
        // Empty Implementation
    }

    @Override
    public void cancel() {
        // Empty Implementation
    }

    @NonNull
    @Override
    public Class<Drawable> getDataClass() {
        return Drawable.class;
    }

    @NonNull
    @Override
    public DataSource getDataSource() {
        return DataSource.LOCAL;
    }
}
