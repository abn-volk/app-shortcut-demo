package net.pnhdroid.appshortcutdemo.iconloader

import android.content.Context
import android.content.pm.ActivityInfo
import android.graphics.drawable.Drawable
import com.bumptech.glide.Glide
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import net.pnhdroid.appshortcutdemo.data.ShortcutInfo

@GlideModule
class AppGlideModule : AppGlideModule() {
    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
        super.registerComponents(context, glide, registry)
        registry.append(ActivityInfo::class.java, Drawable::class.java, DrawableModelLoaderFactory(context))
        registry.append(ShortcutInfo::class.java, Drawable::class.java, ShortcutModelLoaderFactory(context))
    }
}
