package net.pnhdroid.appshortcutdemo.iconloader;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.signature.ObjectKey;

class DrawableModelLoader implements ModelLoader<ActivityInfo, Drawable>
{
    private final Context mContext;

    DrawableModelLoader(Context context) {
        mContext = context;
    }

    @Nullable
    @Override
    public LoadData<Drawable> buildLoadData(@NonNull ActivityInfo applicationInfo, int width, int height, @NonNull Options options) {

        return new LoadData<>( new ObjectKey( applicationInfo ),
                new DrawableDataFetcher( mContext, applicationInfo ) );
    }

    @Override
    public boolean handles(@NonNull ActivityInfo applicationInfo) {
        return true;
    }
}
