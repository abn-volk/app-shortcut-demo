package net.pnhdroid.appshortcutdemo.iconloader;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.signature.ObjectKey;

import net.pnhdroid.appshortcutdemo.data.ShortcutInfo;

class ShortcutModelLoader implements ModelLoader<ShortcutInfo, Drawable>
{
    private final Context mContext;

    ShortcutModelLoader(Context context) {
        mContext = context;
    }

    @Nullable
    @Override
    public LoadData<Drawable> buildLoadData(@NonNull ShortcutInfo applicationInfo, int width, int height, @NonNull Options options) {

        return new LoadData<>( new ObjectKey( applicationInfo ),
                new ShortcutDataFetcher( mContext, applicationInfo ) );
    }

    @Override
    public boolean handles(@NonNull ShortcutInfo applicationInfo) {
        return true;
    }
}
