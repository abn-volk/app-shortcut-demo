package net.pnhdroid.appshortcutdemo

import android.annotation.SuppressLint
import android.content.Context
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

@SuppressLint("ViewConstructor")
class AutoFitRecyclerView(
    context: Context,
    private val itemWidthPx: Int,
) : RecyclerView(context) {

    init {
        layoutManager = GridLayoutManager(context, 5)
    }


    override fun onMeasure(widthSpec: Int, heightSpec: Int) {
        super.onMeasure(widthSpec, heightSpec)
        val width = MeasureSpec.getSize(widthSpec)
        if (width != 0) {
            val spans = width / itemWidthPx
            if (spans > 0) {
                (layoutManager as GridLayoutManager).spanCount = spans
            }
        }
    }


}