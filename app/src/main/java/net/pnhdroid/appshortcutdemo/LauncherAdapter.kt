package net.pnhdroid.appshortcutdemo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import net.pnhdroid.appshortcutdemo.data.LauncherInfo

class LauncherAdapter(
    private val launchers: List<LauncherInfo>,
    private val onClick: (LauncherInfo) -> Unit,
) : RecyclerView.Adapter<LauncherAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vh = ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_launcher, parent, false)
        )
        vh.itemView.setOnClickListener {
            val pos = vh.adapterPosition
            if (pos != RecyclerView.NO_POSITION) {
                onClick(launchers[pos])
            }
        }
        return vh
    }

    override fun getItemCount(): Int = launchers.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val pos = holder.adapterPosition
        if (pos != RecyclerView.NO_POSITION) {
            val launcher = launchers[pos]
            Glide.with(holder.icon)
                .load(launcher.activityInfo)
                .into(holder.icon)
            holder.label.text = launcher.activityInfo.loadLabel(holder.itemView.context.packageManager)
        }
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val icon = itemView.findViewById<ImageView>(R.id.icon)
        val label = itemView.findViewById<TextView>(R.id.label)
    }
}
