package net.pnhdroid.appshortcutdemo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import net.pnhdroid.appshortcutdemo.data.ShortcutInfo

class ShortcutAdapter(
    private val shortcuts: List<ShortcutInfo>,
    private val onClick: (ShortcutInfo) -> Unit,
) : RecyclerView.Adapter<ShortcutAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vh = ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_shortcut, parent, false)
        )
        vh.itemView.setOnClickListener {
            val pos = vh.adapterPosition
            if (pos != RecyclerView.NO_POSITION) {
                onClick(shortcuts[pos])
            }
        }
        return vh
    }

    override fun getItemCount(): Int = shortcuts.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val pos = holder.adapterPosition
        if (pos != RecyclerView.NO_POSITION) {
            val shortcut = shortcuts[pos]
            Glide.with(holder.icon)
                .load(shortcut)
                .into(holder.icon)
            holder.label.text = shortcut.name
        }
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val icon = itemView.findViewById<ImageView>(R.id.icon)
        val label = itemView.findViewById<TextView>(R.id.label)
    }
}
