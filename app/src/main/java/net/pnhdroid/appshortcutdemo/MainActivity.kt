package net.pnhdroid.appshortcutdemo

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.content.res.XmlResourceParser
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import net.pnhdroid.appshortcutdemo.data.LauncherInfo
import net.pnhdroid.appshortcutdemo.data.ShortcutInfo
import org.xmlpull.v1.XmlPullParser
import java.util.UUID


class MainActivity : Activity() {

    companion object {
        private const val XMLNS_ANDROID = "http://schemas.android.com/apk/res/android"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val recyclerView = AutoFitRecyclerView(this, resources.getDimensionPixelSize(R.dimen.grid_item_size))
        setContentView(recyclerView)

        Thread {
            val apps = getAllAppsWithShortcut()
            runOnUiThread {
                recyclerView.adapter = LauncherAdapter(apps) {
                    showShortcuts(it)
                }
            }
        }.start()
    }


    private fun getAllAppsWithShortcut(): List<LauncherInfo> {
        val result = arrayListOf<LauncherInfo>()
        val intent2 = Intent(Intent.ACTION_MAIN)
        intent2.addCategory(Intent.CATEGORY_LAUNCHER)
        for (resolveInfo in packageManager.queryActCompat(intent2, PackageManager.GET_META_DATA)) {
            val activityInfo = resolveInfo.activityInfo

            val shortcutsMetadata = activityInfo.loadXmlMetaData(packageManager, "android.app.shortcuts")
                ?: continue

            shortcutsMetadata.use { parser ->
                val shortcuts = parseShortcuts(activityInfo, parser)
                if (shortcuts.isNotEmpty()) {
                    result.add(LauncherInfo(activityInfo.loadLabel(packageManager).toString(), activityInfo, shortcuts))
                }
            }
        }
        return result
    }

    private fun showShortcuts(launcherInfo: LauncherInfo) = runOnUiThread {

        val dialogView: View = layoutInflater.inflate(R.layout.activity_main, null)
        dialogView.findViewById<RecyclerView>(R.id.recycler_view).apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = ShortcutAdapter(launcherInfo.shortcuts) {
                goToShortcut(this@MainActivity, it)
            }
        }

        AlertDialog.Builder(this)
            .setTitle(launcherInfo.name)
            .setView(dialogView)
            .show()
    }

    private fun PackageManager.queryActCompat(intent: Intent, flags: Int): List<ResolveInfo> =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            this.queryIntentActivities(intent, PackageManager.ResolveInfoFlags.of(flags.toLong()))
        } else {
            this.queryIntentActivities(intent, flags)
        }

    private fun parseShortcuts(actInfo: ActivityInfo, parser: XmlResourceParser): List<ShortcutInfo> {
        val result = arrayListOf<ShortcutInfo>()

        var eventType = parser.eventType
        var currentId: String? = null
        var currentLabel: String? = null
        var currentIconRes = 0
        var currentIntent: Intent? = null
        var currentIntents = arrayListOf<String>()
        while (eventType != XmlPullParser.END_DOCUMENT) {
            when {
                // End of shortcut, add it to the list
                eventType == XmlPullParser.END_TAG && parser.name == "shortcut" -> {
                    if (currentId != null && currentLabel != null && currentIntents.isNotEmpty()) {
                        result.add(ShortcutInfo(currentId, currentLabel, currentIconRes, currentIntents, actInfo.packageName))
                    }
                    currentId = null
                    currentLabel = null
                    currentIconRes = 0
                    currentIntents = arrayListOf()
                }
                // End of intent, add it to the list
                eventType == XmlPullParser.END_TAG && parser.name == "intent" && currentIntent != null -> {
                    currentIntents.add(currentIntent.toUri(Intent.URI_INTENT_SCHEME))
                }
                // Ignore other end tags
                eventType != XmlPullParser.START_TAG -> { }
                // If shortcut has android:enabled="false", ignore it
                parser.name == "shortcut" && parser.getAttributeBooleanValue(XMLNS_ANDROID, "enabled", true) -> {
                    currentId = parser.getAttributeValue("shortcutId") ?: UUID.randomUUID().toString()
                    var labelRes = parser.getAttributeResourceValue("shortcutShortLabel")
                    if (labelRes == 0) {
                        labelRes = parser.getAttributeResourceValue("shortcutLongLabel")
                    }
                    currentLabel = if (labelRes == 0) {
                        null
                    } else {
                        val resources = packageManager.getResourcesForApplication(actInfo.packageName)
                        resources.getString(labelRes)
                    }
                    currentIconRes = parser.getAttributeResourceValue("icon")
                }
                // If shortcut is disabled, currentId should be null. Ignore <intent>s.
                parser.name == "intent" && currentId != null -> {
                    try {
                        val shortcutIntent = Intent().apply {
                            action = parser.getAttributeValue("action")
                            val pkg = parser.getAttributeValue("targetPackage")
                            val cls = parser.getAttributeValue("targetClass")
                                ?: parser.getAttributeValue("targetActivity")?.replace('$', '_')
                            if (pkg != null && cls != null) {
                                setClassName(pkg, cls)
                            }
                            // Use last activity name as shortcut label
                            // Workaround for older versions where some XML attrs are unavailable (shortcutShortLabel...)
                            currentLabel = currentLabel ?: cls?.substringAfterLast('.')
                            data = parser.getAttributeValue("data")?.let(Uri::parse)
                        }
                        currentIntent = shortcutIntent
                    } catch (_: Exception) { }
                }
                // Intent extra. Just assume it is a String
                parser.name == "extra" && currentIntent != null -> {
                    val name = parser.getAttributeValue("name")
                    val value = parser.getAttributeValue("value")
                    if (name != null && value != null) {
                        currentIntent.putExtra(name, value)
                    }
                }
                // Intent categories
                parser.name == "categories" && currentIntent != null -> {
                    val name = parser.getAttributeValue("name")
                    if (name != null) {
                        currentIntent.addCategory(name)
                    }
                }
            }
            eventType = parser.next()
        }

        return result
    }

    /**
     * These two methods try to get attributes with and without the android: namespace
     * They are needed because some shortcuts.xml (e.g. Chrome) doesn't use namespaces
     */
    private fun XmlResourceParser.getAttributeValue(attribute: String): String? {
        return getAttributeValue(XMLNS_ANDROID, attribute) ?: getAttributeValue(null, attribute)
    }

    private fun XmlResourceParser.getAttributeResourceValue(attribute: String): Int {
        val res = getAttributeResourceValue(XMLNS_ANDROID, attribute, 0)
        if (res != 0) {
            return res
        }
        return getAttributeResourceValue(null, attribute, 0)
    }


    private fun goToShortcut(context: Context, shortcut: ShortcutInfo) = runOnUiThread {
        try {
            val intents = shortcut.intents.mapIndexed { index, uri ->
                Intent.parseUri(uri, Intent.URI_INTENT_SCHEME).apply {
                    if (index == 0) {
                        if (action == null) {
                            action = Intent.ACTION_MAIN
                            addCategory(Intent.CATEGORY_LAUNCHER)
                        }
                        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    }
                }
            }.toTypedArray()
            context.startActivities(intents)
        } catch (e: Exception) {
            AlertDialog.Builder(this)
                .setTitle(e.javaClass.simpleName)
                .setMessage(e.message)
                .show()
        }
    }
}
