package net.pnhdroid.appshortcutdemo.data

import android.content.pm.ActivityInfo

class LauncherInfo (
    val name: String,
    val activityInfo: ActivityInfo,
    val shortcuts: List<ShortcutInfo>,
)

class ShortcutInfo(
    val id: String,
    val name: String,
    val iconRes: Int,
    val intents: List<String>,
    val packageName: String,
)
